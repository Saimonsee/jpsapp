package com.hellokoding.springboot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Usuarios {

	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private String nick;
	private String mail;
	private String pass;
	
	@Transient
	private List<String> rol;
	
	public Usuarios() {}
	
	public Usuarios(String nombre, String pass) {
		super();
		this.nombre = nombre;
		this.pass = pass;
	}

	@OneToMany(cascade = CascadeType.ALL,
			   mappedBy= "user")
	private List<Opiniones> opiniones;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<String> getRol() {
		return rol;
	}

	public void setRol(List<String> rol) {
		this.rol = rol;
	}

	public List<Opiniones> getOpiniones() {
		return opiniones;
	}

	public void setOpiniones(List<Opiniones> opiniones) {
		this.opiniones = opiniones;
	}
	
	
}
