package com.hellokoding.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hellokoding.springboot.model.Usuarios;

@Repository
public interface UsuarioDao extends JpaRepository<Usuarios, Long> {

}
