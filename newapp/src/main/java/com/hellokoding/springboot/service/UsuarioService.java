package com.hellokoding.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hellokoding.springboot.model.Usuarios;
import com.hellokoding.springboot.repository.UsuarioDao;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;
	
	public void save(Usuarios s) {
		usuarioDao.save(s);
	}
	
	public List<Usuarios> findAll(){
		return usuarioDao.findAll();
	}
}
