package com.hellokoding.springboot.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hellokoding.springboot.model.Usuarios;
import com.hellokoding.springboot.repository.UsuarioDao;
import com.hellokoding.springboot.service.UsuarioService;

@Controller
public class HelloController {
	
		@Autowired
		UsuarioService usuarioService;
	
	    @GetMapping({"/", "/hello"})
	    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
	        model.addAttribute("name", name);
	        usuarioService.save(new Usuarios("test", "test"));
	        System.out.println("Grabado!");
	        System.out.println("Elementos"+usuarioService.findAll().size());
	        return "hello";
	    }
	    

	    @GetMapping("/login")
	    public String cualquiera(String user, String pass,Model model) {
	    	
	    	return "login"; 
	    }

	    
	    @PostMapping("/login")
	    public String login(@RequestParam String user, String pass, Model model) {
	    	boolean validacion=true;
	    	String msg="";
	    	
	    	Usuarios usuario = new Usuarios("simon@gmail.com","fernando");
	    	if(user.equals(usuario.getNombre())) {
	    		if(pass.equals(usuario.getPass())){
	    			System.out.println("el usuario y contraseña son correctos");
	    			msg = "el usuario y contraseña son correctos";
	    			
	    		}else {
	    			System.out.println("no se pudo validar tu usuario");
	    			validacion=false;
	    			msg = "no se pudo validar tu usuario";
	    		}
	    	}else {
	    		System.out.println("no se ha encontrado ese user");
	    		msg = "no se ha encontrado ese user";
	    		validacion=false;
	    	}
	    	model.addAttribute(validacion);
	    	model.addAttribute("mensaje",msg);
	    		
	    System.out.println(user.equals(pass));
	    System.out.println(validacion);
	    	
	    	return "login";
	    	
	    }
	    
}


