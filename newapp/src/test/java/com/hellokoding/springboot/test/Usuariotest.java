package com.hellokoding.springboot.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hellokoding.springboot.model.Usuarios;
import com.hellokoding.springboot.repository.UsuarioDao;

@RunWith(SpringRunner.class)
@DataJpaTest
public class Usuariotest {
	
	@Autowired
	private UsuarioDao usuariodao;
	private Long id = -1L;
	
	@Test
	public void insertarUsuario() {
		Usuarios user = new Usuarios("Franco","contraseña");
		
		usuariodao.save(user);
		System.out.println("se guardo usuario");
	}
	
	@Test
	public void listarUsuario() {
		
		List<Usuarios> usuarios = usuariodao.findAll();
		
		for (Usuarios user : usuarios) {
			 System.out.println(user.getNombre());
		}
	}
	
	@Test
	public void borrarUsuario() {
		
		usuariodao.deleteById(id);
		System.out.println("el usuario fue borrado");
		
	}

}
